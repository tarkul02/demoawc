import React, { Component } from "react";
import { DropDownList } from "@progress/kendo-react-dropdowns";
import categories from "../dataMaster/categories.json";
import datatstep from "../dataMaster/step.json";
import {
  DatePicker
} from "@progress/kendo-react-dateinputs";
import { Checkbox } from "@progress/kendo-react-inputs";
import { Editor, EditorTools } from "@progress/kendo-react-editor";
import { Upload } from "@progress/kendo-react-upload";
import { Button } from '@progress/kendo-react-buttons'
import { Stepper } from "@progress/kendo-react-layout";
import { Dialog, DialogActionsBar } from "@progress/kendo-react-dialogs";
import {
  Grid,
  GridColumn as Column,
  GridToolbar,
} from '@progress/kendo-react-grid'

export default class Header extends Component {

  state = {
    value: 0,
    visible:false,
    setVisible:[],
  };
  
  handleChange = (e) => {
    this.setState({
      value: e.value,
    });
  };
   toggleDialog = () => {
    this.setState({
      visible: !this.state.visible
    });
  };
  render() {
    
    const {
      Bold,
      Italic,
      Underline,
      Strikethrough,
      AlignLeft,
      AlignCenter,
      AlignRight,
      AlignJustify,
      Indent,
      Outdent,
      OrderedList,
      UnorderedList,
      Undo,
      Redo,
      FontSize,
      FontName,
      FormatBlock,
      Link,
      Unlink,
      InsertImage,
      ViewHtml,
      InsertTable,
    } = EditorTools;

    

    return (
      
      <div className="container pl-0 pr-0">
        <div className="text-center mt-5 mb-5 header-text">ขออนุมัติปล่อยเช่าพื้นที่ (Quotation / Contract)</div>
        <div className="boxlist" style={{borderRadius:"10px"}}>
          <div className="body-content" style={{borderRadius:"10px"}}>
          <Stepper
            value={this.state.value}
            onChange={this.handleChange}
            items={datatstep}
          />
          </div>
        </div>
        <div className="boxlist box1">
          <div className="headtext">รายละเอียดผู้สรา้ง (Requester Detail)</div>
          <div className="body-content">
            <div className="row">
              <div className="col-md-6">
              <table className="table table-bordered">
                <tbody>
                  <tr>
                    <td width={200}><p>Quotation No. (รหัสเอกสาร):</p></td>
                    <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                  </tr>
                  <tr>
                    <td><p>Requester (ชื่อผู้สร้าง):</p></td>
                    <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                  </tr>
                  <tr>
                    <td><p>Bu./Branch (หน่วยธุระกิจ/สาขา):</p></td>
                    <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                  </tr>
                </tbody>
              </table>
              </div>
              <div className="col-md-6">
                <table className="table table-bordered">
                  <tbody>
                    <tr>
                      <td width={160}><p>Create Date (วันที่สร้าง):</p></td>
                      <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                    </tr>
                    <tr>
                      <td><p>Com code (บริษัท):</p></td>
                      <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                    </tr>
                    <tr>
                      <td><p>Sale Name (ชื่อผู้ชาย):</p></td>
                      <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
        <div className="boxlist box2">
          <div className="headtext">รายละเอียดผู้เช่า (Custumer Detail)</div>
          <div className="body-content">
            <div className="row">
              <div className="col-md-6">
                <table className="table table-bordered">
                  <tbody>
                    <tr>
                      <td width={200}>
                        <Button className="btn btn-primary k-mr-1" icon="search">search</Button>
                        <Button className="btn btn-danger" icon="refresh">Reset</Button>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div className="col-md-6">
                <table className="table table-bordered">
                  <tbody>
                    <tr>
                      <td width={250}><p>Business partner Code  (รหัสลูกค้า (BP)):</p></td>
                      <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
            <div className="row">
              <div className="col-md-6">
                <table className="table table-bordered">
                  <tbody>
                    <tr>
                      <td width={250}><p>Custumer Name  (ชื่อผู้เช่า):</p></td>
                      <td>
                        <DropDownList
                            data={categories}
                            dataItemKey="CategoryID"
                            textField="CategoryName"
                          />
                      </td>
                    </tr>
                    <tr>
                      <td width={250}><p>Company Name (ชื่อสถานประกอกการ):</p></td>
                      <td>
                        <DropDownList
                          data={categories}
                          dataItemKey="CategoryID"
                          textField="CategoryName"
                        />
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div className="col-md-6">
                <table className="table table-bordered">
                  <tbody>
                    <tr>
                      <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                    </tr>
                    <tr>
                      <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
            <div className="row">
              <div className="col-md-6">
                <table className="table table-bordered">
                  <tbody>
                    <tr>
                      <td width={250}><p>Shop Name (ชื่อร้านค้า):</p></td>
                      <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                    </tr>
                    <tr>
                      <td><p style={{padding: "0px 0px"}}>Company certificater No (หนังสือรับรองเลขที่):</p></td>
                      <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                    </tr>
                    <tr>
                      <td><p>Certificater Date (วันที่ออกหนังสือรับรอง):</p></td>
                      <td>
                        <DatePicker />
                      </td>
                    </tr>
                    <tr>
                      <td><p>Telephone (เบอร์ติดต่อ):</p></td>
                      <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                    </tr>
                    <tr>
                      <td><p>Email (อีเมล):</p></td>
                      <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                    </tr>
                    <tr>
                      <td><p>Business Type (ประเภทธุรกิจ):</p></td>
                      <td>
                        <DropDownList
                            data={categories}
                            dataItemKey="CategoryID"
                            textField="CategoryName"
                          />
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div className="col-md-6">
                <table className="table table-bordered">
                  <tbody>
                    <tr>
                      <td width={250}><p>Presonal ID (เลขที่บัตรประชาชน):</p></td>
                      <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                    </tr>
                    <tr>
                      <td><p>Taxpayer Number (เลขที่ผู้เสียภาษี):</p></td>
                      <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                    </tr>
                    <tr>
                      <td><p>Registerd capital (ทุนจดทะเบียน):</p></td>
                      <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                    </tr>
                    <tr>
                      <td><p>Registerd number (ทะเบียนเลขที่):</p></td>
                      <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                    </tr>
                    <tr>
                      <td><p>Special Reason (For BI):</p></td>
                      <td>
                        <DropDownList
                            data={categories}
                            dataItemKey="CategoryID"
                            textField="CategoryName"
                          />
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
            <div className="row">
              <div className="col-md-12">
                <table className="table table-bordered">
                  <tbody>
                    <tr>
                      <td width={250}><p>Sub Business Type (ธุระกิจ):</p></td>
                      <td></td>
                      <td></td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>

            {/* table Sub Business Type (ธุระกิจ) */}
            <div className="row" style={{maxWidth:"900px"}}>
              <div className="col-md-12">
                <table className="table table-bordered tableboxshodow background1 " >
                  <tbody>
                    <tr className="color1">
                      <td><p className="padding1">No</p></td>
                      <td style={{textAlign:"center"}}><p>BUSINESS TYPE</p></td>
                      <td style={{textAlign:"center"}}><p>PERCENTAGE</p></td>
                    </tr>
                    <tr>
                      <td width={150}><p className="padding1">1</p></td>
                      <td width={400}>
                        <DropDownList
                          data={categories}
                          dataItemKey="CategoryID"
                          textField="CategoryName"
                        />  
                      </td>
                      <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                    </tr>
                    <tr>
                      <td><p className="padding1">2</p></td>
                      <td>
                        <DropDownList
                          data={categories}
                          dataItemKey="CategoryID"
                          textField="CategoryName"
                        />  
                      </td>
                      <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                    </tr>
                    <tr>
                      <td><p className="padding1">3</p></td>
                      <td>
                        <DropDownList
                          data={categories}
                          dataItemKey="CategoryID"
                          textField="CategoryName"
                        />  
                      </td>
                      <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
            
            {/* address */}
            <div className="row mt-3">
              <div className="col-md-12">
                <Button className="btn btn-primary k-mr-1" icon="search">More</Button>
                <Button className="btn btn-success " icon="plus">New</Button>
              </div>
              <div className="row">
                <div className="col-md-4">
                  <table className="table table-bordered">
                    <tbody>
                      <tr>
                        <td width={150}><p>Address(เลขที่):</p></td>
                        <td>
                          <input type="text" className="form-control" aria-describedby="basic-addon3"/>
                        </td>
                      </tr>
                      <tr>
                        <td ><p>Road (ถนน):</p></td>
                        <td>
                          <input type="text" className="form-control" aria-describedby="basic-addon3"/>
                        </td>
                      </tr>
                      <tr>
                        <td ><p style={{padding: "0px 0px"}}>Sub-district (ตำบล/แขวง):</p></td>
                        <td>
                          <input type="text" className="form-control" aria-describedby="basic-addon3"/>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                <div className="col-md-4">
                  <table className="table table-bordered">
                    <tbody>
                      <tr>
                        <td width={150}><p>Moo (หมูที่):</p></td>
                        <td>
                          <input type="text" className="form-control" aria-describedby="basic-addon3"/>
                        </td>
                      </tr>
                      <tr>
                        <td ><p>Province (จังหวัด):</p></td>
                        <td>
                          <input type="text" className="form-control" aria-describedby="basic-addon3"/>
                        </td>
                      </tr>
                      <tr>
                        <td ><p style={{padding: "0px 0px"}}>Zip Code (รหัสโปรษณีย์):</p></td>
                        <td>
                          <input type="text" className="form-control" aria-describedby="basic-addon3"/>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                <div className="col-md-4">
                  <table className="table table-bordered">
                    <tbody>
                      <tr>
                        <td width={150}><p>Soi (ซอย):</p></td>
                        <td>
                          <input type="text" className="form-control" aria-describedby="basic-addon3"/>
                        </td>
                      </tr>
                      <tr>
                        <td ><p>District (อำเภอ/เขต):</p></td>
                        <td>
                          <input type="text" className="form-control" aria-describedby="basic-addon3"/>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="boxlist">
          <div className="headtext">Consent Management System (CMS)</div>
          <div className="body-content">
            <div className="row">
              <div className="col-md-12">
                <table className="table table-bordered tableborder">
                  <thead>
                    <tr>
                      <th scope="col"><p className="padding1">Custumer Mobile</p></th>
                      <th scope="col"><p className="padding1">Custumer Name</p></th>
                      <th scope="col"><p className="padding1">Status</p></th>
                      <th scope="col"><p className="padding1">Consent Date</p></th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td><p className="padding1">0823445678</p></td>
                      <td><p className="padding1">ญาณ์กาญจน์ ม่วงน้อยเจริญ</p></td>
                      <td><p className="padding1">Consent Success</p></td>
                      <td><p className="padding1">27-10-2022</p></td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
       <div className="boxlist box3">
          <div className="headtext">รายละเอียดพื้นที่ผู้เช่า (Spece Detail)</div>
          <div className="body-content">
            <div className="row">
              <div className="col-md-6 box1">
                <table className="table table-bordered">
                  <tbody>
                    <tr>
                      <td width={250}><p>Contract No. (เลขที่สัญญา):</p></td>
                      <td ><input style={{width:"226.9px"}}  type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                    </tr>
                    <tr>
                      <td><p>Contract Start date (วันที่เริ่มต้นสัญญา):</p></td>
                      <td>
                        <DatePicker />  
                      </td>
                    </tr>
                    <tr>
                      <td><p>Expiration (วันที่สิ้นสุดนำเสนอ):</p></td>
                      <td>
                        <DatePicker 
                        />  
                      </td>
                    </tr>
                    <tr>
                      <td width={250}><p>Spece Detail (พื้นที่เช่า):</p></td>
                      <td><Button className="btn btn-primary k-mr-1" icon="search">search</Button></td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div className="col-md-6 box1">
                <table className="table table-bordered ">
                  <tbody>
                    <tr>
                      <td width={300}><p>Refer Contract No. (อ้างอิงเลขที่เอาสัญญา):</p></td>
                      <td><input  style={{width:"226.9px"}} type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                    </tr>
                    <tr>
                      <td><p>Contract End date (วันที่สิ้นสุดสัญญา):</p></td>
                      <td>
                        <DatePicker />
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div className="col-md-12 ">
                <table className="table table-bordered tableborder">
                  <thead>
                    <tr>
                      <th scope="col"><p className="padding1">Unit No.<br/></p></th>
                      <th scope="col"><p className="padding1">Room No.</p></th>
                      <th scope="col"><p className="padding1">Building</p></th>
                      <th scope="col"><p className="padding1">Condition</p></th>
                      <th scope="col"><p className="padding1">Floot</p></th>
                      <th scope="col"><p className="padding1">Zone</p></th>
                      <th scope="col"><p className="padding1">Area Type</p></th>
                      <th scope="col"><p className="padding1">Unit Price</p></th>
                      <th scope="col"><p className="padding1">Area</p></th>
                      <th scope="col"><p className="padding1">1</p></th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td><p className="padding1">8247/PNT-PN/4110275</p></td>
                      <td><p className="padding1">250</p></td>
                      <td><p className="padding1">Plaza</p></td>
                      <td><p className="padding1">Z000</p></td>
                      <td><p className="padding1">2</p></td>
                      <td><p className="padding1">Techrญ</p></td>
                      <td><p className="padding1">Shop</p></td>
                      <td><p className="padding1">1,930.95</p></td>
                      <td><p className="padding1">13.12</p></td>
                      <td>
                        <p className="padding1">
                            <i className="fa-regular fa-circle-xmark"></i>
                        </p>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div className="col-md-6 mt-4 box1">
                <table className="table table-bordered">
                  <tbody>
                    <tr>
                      <td width={200}><p>Total Spece(พื้นที่รวม):</p></td>
                      <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                      <td width={120}><p>Sqm(ตรม.)</p></td>
                    </tr>
                    <tr>
                      <td><p>Contract Type. (ประเภทสัญญา):</p></td>
                      <td>
                        <DropDownList
                          data={categories}
                          dataItemKey="CategoryID"
                          textField="CategoryName"
                        />  
                      </td>
                      <td><p></p></td>
                    </tr>
                    <tr>
                      <td><p className="padding2">Model (ประเภทโครงสรา้งรายได้):</p></td>
                      <td>
                        <DropDownList
                          data={categories}
                          dataItemKey="CategoryID"
                          textField="CategoryName"
                        />  
                      </td>
                      <td><p></p></td>
                    </tr>
                    <tr>
                      <td ><p>Contract Period (สัญญาเช่า):</p></td>
                      <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                      <td><p></p></td>
                    </tr>
                    <tr>
                      <td><p className="padding2">Motorcycle parking (จำนวนที่จอดจักรยานยนต์):</p></td>
                      <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                      <td><p>Motorcycle (คัน)</p></td>
                    </tr>
                    <tr>
                      <td><p className="padding2">Purpose of leasing (วัตถุประสงค์):</p></td>
                      <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                      <td><p></p></td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div className="col-md-6 mt-4 box1">
                <table className="table table-bordered">
                  <tbody>
                    <tr>
                      <td width={200}><p className="padding2">Average Price List (ราคาขายต่อ ตรม.):</p></td>
                      <td ><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                      <td width={120}><p>Baht(บาท)</p></td>
                    </tr>
                    <tr>
                      <td><p className="padding2">Rental Type (ประเภทโครงสร้าง):</p></td>
                      <td>
                        <DropDownList
                          data={categories}
                          dataItemKey="CategoryID"
                          textField="CategoryName"
                          width="100%"
                        />  
                      </td>
                      <td><p></p></td>
                    </tr>
                    <tr>
                      <td><p>Decorate (ระยะเวลาตกแต่ง):</p></td>
                      <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                      <td><p>Day (วัน)</p></td>
                    </tr>
                    <tr>
                      <td><p>Dellver date (วันที่ส่งมอบ):</p></td>
                      <td>
                        <DatePicker
                         width="100%"
                         />
                        
                      </td>
                      <td><p></p></td>  
                    </tr>
                    <tr>
                      <td><p className="padding2">Car parking (จำนวนที่จอดรถยนต์):</p></td>
                      <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                      <td><p>Car (คัน)</p></td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div className="col-md-12 mt-3">
                <Button className="btn btn-primary k-mr-1" icon="calculator" style={{float:"right"}}>Calulate</Button>
              </div>
              <div className="col-md-12 mt-3">
                <div><p className="subheadtext">Remark (หมายเหตุ)</p></div>
                <div>
                  <Editor
                    tools={[
                      [Bold, Italic, Underline, Strikethrough],
                      [AlignLeft, AlignCenter, AlignRight, AlignJustify],
                      [Indent, Outdent],
                      [OrderedList, UnorderedList],
                      FontSize,
                      FontName,
                      FormatBlock,
                      [Undo, Redo],
                      [Link, Unlink, InsertImage, ViewHtml],
                      [InsertTable],
                    ]}
                    contentStyle={{
                      height: 200,
                    }}
                  />
                </div>
              </div>
              <div className="col-md-12 mt-5">
                <div><p className="subheadtext">Spece Co (หมายเหตุ)</p></div>
                <div>
                  <Editor
                    tools={[
                      [Bold, Italic, Underline, Strikethrough],
                      [AlignLeft, AlignCenter, AlignRight, AlignJustify],
                      [Indent, Outdent],
                      [OrderedList, UnorderedList],
                      FontSize,
                      FontName,
                      FormatBlock,
                      [Undo, Redo],
                      [Link, Unlink, InsertImage, ViewHtml],
                      [InsertTable],
                    ]}
                    contentStyle={{
                      height: 200,
                    }}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="boxlist">
          <div className="headtext">Recognizance (เงินประกัน)</div>
          <div className="body-content">
            <div className="row">
              <div className="col-md-12">
                <div className="background1">
                  <table className="table table-bordered">
                    <tbody>
                      <tr>
                        <td width={300}><p>Recognizance Month. (จำนวนเดือนเงินประกัน)</p></td>
                        <td width={100}><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                        <td><p>Month (เดือน): <span className="classspan">* ระบบจะนำจำนวนเดือนไปคิดกับ Rate จากปีสุดท้าย</span></p></td>
                        <td><Button className="btn btn-primary k-mr-1" style={{float:"right"}} icon="calculator">Calulate</Button></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
              <div className="col-md-12 ">
                <div className="background2 k-p-5">
                  <table className="table table-bordered background2">
                    <thead>
                      <tr>
                        <th scope="col"><p >Deposit<br/></p></th>
                        <th scope="col"><p >AMT Before Vat</p></th>
                        <th scope="col"><p >Vat 7%</p></th>
                        <th scope="col"><p >Total AMT</p></th>
                        <th scope="col"><p >Remark</p></th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td width={350}><p>Rental Deposit/เงินประกันการเช่า :</p></td>
                        <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                        <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                        <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                        <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                      </tr>
                      <tr>
                        <td><p>Service Deposit/เงินประกันการบริการ :</p></td>
                        <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                        <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                        <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                        <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                      </tr>
                      <tr>
                        <td><p>Pubile Service Deposit/เงินประกันสาธารณุปโภคอื่นๆ :</p></td>
                        <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                        <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                        <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                        <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
              <div className="col-md-12 k-mt-10">
                <div className="background2 k-p-5">
                  <table className="table table-bordered background2">
                    <thead>
                      <tr>
                        <th scope="col"><p >Other Utilities Deposit<br/></p></th>
                        <th scope="col"><p >AMT Before Vat</p></th>
                        <th scope="col"><p >Vat 7%</p></th>
                        <th scope="col"><p >Total AMT</p></th>
                        <th scope="col"><p >Remark</p></th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td width={200}>
                          <DropDownList
                            data={categories}
                            dataItemKey="CategoryID"
                            textField="CategoryName"
                          />  
                        </td>
                        <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                        <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                        <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                        <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
              <div className="col-md-12 k-mt-10"><p className="subheadtext">Utility Deposit/ประกันการใช้สาธารณุประโภค :<br/></p></div>
              <div className="col-md-12 ">
                <div className="background2 k-p-5">
                  <table className="table table-bordered ">
                    <thead>
                      <tr>
                        <th scope="col"><p >Merter No.</p></th>
                        <th scope="col"><p >Room No.</p></th>
                        <th scope="col"><p >Type</p></th>
                        <th scope="col"><p >Amount</p></th>
                      </tr>
                    </thead>
                  </table>
                </div>
              </div>
              <div className="col-md-6 mt-4"></div>
              <div className="col-md-6 mt-4 box1">
                <table className="table table-bordered">
                  <tbody>
                    <tr>
                      <td><p className="padding2">Deposit Amount (รวมเงินประกัน):</p></td>
                      <td width={150}><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                      <td width={120}><p>Baht(บาท)</p></td>
                    </tr>
                    <tr>
                      <td><p className="padding2">Vat 7% (ภาษี 7%):</p></td>
                      <td width={150}><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                      <td width={120}><p>Baht(บาท)</p></td>
                    </tr>
                    <tr>
                      <td><p className="padding2">Total Deposit (รวมเงินประกันทั้งหมด)</p></td>
                      <td width={150}><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                      <td width={120}><p>Baht(บาท)</p></td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div className="col-md-12 k-mt-10"><p className="subheadtext">ค่าใช่จ่ายอื่นๆ (Other Expenses) :<span className="classspan">* กรณีหน่วยมิเตอร์ใช้ตาม Rate ปกติไม่ต้องกรอกข้อมูล </span></p></div>
              <div className="col-md-12 ">
                <div className="background2 k-p-5">
                  <table className="table table-bordered background2">
                    <thead>
                      <tr>
                        <th scope="col"><p>Expenses</p></th>
                        <th scope="col"><p>AMT Before Vat</p></th>
                        <th scope="col"><p>VAT</p></th>
                        <th scope="col"><p>VAT 7%</p></th>
                        <th scope="col"><p>Amount</p></th>
                        <th scope="col"><p>Remark</p></th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td width={350}><p>Tableware Serv Fee/ค่าบริการภาชนะ(Tableware)</p></td>
                        <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                        <td width={100}>
                          <DropDownList
                            data={categories}
                            dataItemKey="CategoryID"
                            textField="CategoryName"
                          />  
                        </td>
                        <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                        <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                        <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                      </tr>
                      <tr>
                        <td><p>Electricity MeterFee/ค่าบำรุงมิเตอร์ย่อยไฟฟ้า</p></td>
                        <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                        <td>
                          <DropDownList
                            data={categories}
                            dataItemKey="CategoryID"
                            textField="CategoryName"
                          />  
                        </td>
                        <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                        <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                        <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                      </tr>
                      <tr>
                        <td><p>Water Meter Fee/ค่าบำรุงมิเตอร์ค่าประปา</p></td>
                        <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                        <td>
                          <DropDownList
                            data={categories}
                            dataItemKey="CategoryID"
                            textField="CategoryName"
                          />  
                        </td>
                        <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                        <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                        <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                      </tr>
                      <tr>
                        <td><p>Fixed:Water/ค่าน้ำอัตราเหมา</p></td>
                        <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                        <td>
                          <DropDownList
                            data={categories}
                            dataItemKey="CategoryID"
                            textField="CategoryName"
                          />  
                        </td>
                        <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                        <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                        <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                      </tr>
                      <tr>
                        <td><p>Storage Service/ค่าบรการพื้นที่ Storage</p></td>
                        <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                        <td>
                          <DropDownList
                            data={categories}
                            dataItemKey="CategoryID"
                            textField="CategoryName"
                          />  
                        </td>
                        <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                        <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                        <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
              <div className="col-md-12 k-mt-10"><p className="subheadtext">Advance Rental and Service/ค่าเช่าและค่าบริการล่วงหน้า<br/></p></div>
              <div className="col-md-12 ">
                <div className="background2 k-p-5">
                  <table className="table table-bordered ">
                    <thead>
                      <tr>
                        <th scope="col"><p>Advance Item</p></th>
                        <th scope="col"><p>AMT Before Vat</p></th>
                        <th scope="col"><p>VAT 7%</p></th>
                        <th scope="col"><p>Total Amount</p></th>
                        <th scope="col"><p>Remark</p></th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td width={350}><p>Advance Rental/ค่าเช่าล่วงหน้า</p></td>
                        <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                        <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                        <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                        <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                      </tr>
                      <tr>
                        <td><p>Advance Service/ค่าบริการล่วงหน้า</p></td>
                        <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                        <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                        <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                        <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                      </tr>
                      <tr>
                        <td><p>Advance Pubile Service/ค่าบริการสาธารณะส่วนกลางล่วงหน้า</p></td>
                        <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                        <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                        <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                        <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                      </tr>
                      <tr>
                        <td><p>Advance Property Tex/ค่าภาษีโรงเรือนล่วงหน้า</p></td>
                        <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                        <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                        <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                        <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                      </tr>
                    </tbody>
                  </table>
                  <div className="totaltable k-mt-10">Total Advance (รวมค่าเช่า/ค่าบริการล่วงหน้า)  :<input style={{width:200}}  type="text" className="form-control" aria-describedby="basic-addon3"/>  :  Bath(บาท)</div>
                </div>
              </div>
              <div className="col-md-12 k-mt-10"><p className="subheadtext">Strmp Duty (ค่าอากรแสตมป์) :<br/></p></div>
              <div className="col-md-6 box1 ">
                <table className="table table-bordered ">
                  <tbody>
                    <tr>
                      <td width={250}><p>Rental Contract (สัญญาเช่าพื้นที่):</p></td>
                      <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                    </tr>
                    <tr>
                      <td><p className="padding2">Pubile Service Contract (สัญญาบริการสาธารณุปโภค):</p></td>
                      <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div className="col-md-6 box1">
                <table className="table table-bordered">
                  <tbody>
                    <tr>
                      <td width={250}><p>Service Contract (สัญญาบริการพื้นที่):</p></td>
                      <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                    </tr>
                    <tr>
                      <td><p>Copy (จำนวนคู่ฉบับ):</p></td>
                      <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                    </tr>
                  </tbody>
                </table>
              </div>

              <div className="col-md-12 k-mt-10"><p className="subheadtext">Payment Amount (ยอดเงินเรียกเก็บ):<br/></p></div>
              <div className="col-md-6 box1">
                <table className="table table-bordered">
                  <tbody>
                    <tr>
                      <td width={250}><p>Total Amt (รวมเป็นเงินที่ต้องชำระทั้งสิ้น):</p></td>
                      <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                      <td width={100}><p>Bath (บาท)</p></td>
                    </tr>
                    <tr>
                      <td><p>Old Deposit (เงินประกันเต็ม):</p></td>
                      <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                      <td><p>Bath (บาท)</p></td>
                    </tr>
                    <tr>
                      <td><p>Additional Deposit (เงินประกันส่วนเพิ่ม):</p></td>
                      <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                      <td><p>Bath (บาท)</p></td>
                    </tr>
                    <tr>
                      <td><p>Total Starmp Duty (รวมค่าอากรแสตมป์):</p></td>
                      <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                      <td><p>Bath (บาท)</p></td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div className="col-md-6 box1">
                <table className="table table-bordered">
                  <tbody>
                    <tr>
                      <td width={250}><p>Grand Total (รวมยอดที่ต้องชำระ):</p></td>
                      <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                      <td width={100}><p>Bath (บาท)</p></td>
                    </tr>
                    <tr>
                      <td><p className="padding2">Place a Reservation #1 (วงเงินของงวดที่ 1):</p></td>
                      <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                      <td><p></p></td>
                    </tr>
                    <tr>
                      <td><p>With in (ภายในวันที่)</p></td>
                      <td>
                        <DatePicker/>
                      </td>
                      <td><p>Bath (บาท)</p></td>
                    </tr>
                    <tr>
                      <td><p className="padding2">Pay the Rest (ชำระส่วนที่เหลืองวดที่ 2):</p></td>
                      <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                      <td><p></p></td>
                    </tr>
                    <tr>
                      <td><p>With in (ภายในวันที่)</p></td>
                      <td>
                        <DatePicker/>
                      </td>
                      <td><p></p></td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div>
                <Button className="btn btn-primary k-mt-5" style={{float:"right"}} icon="calculator">Calulate</Button>
              </div>
            </div>
          </div>
        </div>
        <div className="boxlist">
          <div className="headtext">เอกสารแนบ (Document)</div>
          <div className="body-content">
            <div className="row">
              <div className="col-md-12 k-mt-10"><p className="subheadtext">Contract Document registeation/เอกสารประกอบการทำสัญญาเช่า<br/></p></div>
              <div className="col-md-12">
                <table className="table table-bordered ">
                  <thead>
                    <tr>
                      <th scope="col"><p>Document type</p></th>
                      <th scope="col"><p>Value</p></th>
                      <th scope="col"><p>Recieced</p></th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td><p>Copy od house registeation/สำเนาทะเบียนบ้าน</p></td>
                      <td width={200}>2</td>
                      <td><Checkbox/></td>
                    </tr>
                    <tr>
                      <td><p>ID Card/บัตรประจำตัวประชาชน</p></td>
                      <td>2</td>
                      <td><Checkbox/></td>
                    </tr>
                    <tr>
                      <td><p>Passport Copy/สำเนาหนังสือเดินทาง</p></td>
                      <td>1</td>
                      <td><Checkbox/></td>
                    </tr>
                    <tr>
                      <td><p>Work Pemit Copy/สำเนา Work Pemit</p></td>
                      <td>1</td>
                      <td><Checkbox/></td>
                    </tr>
                    <tr>
                      <td><p>Phor Por 20/ภ.พ.20</p></td>
                      <td>1</td>
                      <td><Checkbox/></td>
                    </tr>
                  </tbody>
                </table>
              </div>      
            </div>
            <div className="row">
              <div className="col-md-12 k-mt-10"><p>Other Documents (เอาสารอื่นๆ)<br/></p></div>
              <div className="col-md-12">
                
               
                {/* <div>
                  <button
                    className="k-button k-button-md k-rounded-md k-button-solid k-button-solid-base"
                    onClick={this.toggleDialog}
                  >
                    Open Dialog
                  </button>
                  {this.state.visible && (
                    <Dialog title={"Please confirm"} onClose={this.toggleDialog}>
                      {this.state.visible && (
                        <Dialog title={"Please confirm"} onClose={this.toggleDialog}>
                          <Upload
                            autoUpload={false}
                            defaultFiles={[]}
                            withCredentials={false}
                            // saveUrl={"https://demos.telerik.com/kendo-ui/service-v4/upload/save"}
                            // removeUrl={"https://demos.telerik.com/kendo-ui/service-v4/upload/remove"}
                          />
                        </Dialog>
                      )}
                      <p
                        style={{
                          margin: "25px",
                          color:"red",
                        }}
                      >
                        * Maximum upload file size: 100 MB.
                      </p>
                    </Dialog>
                  )}
                </div> */}
                <Grid
                  
                >
                  <GridToolbar>
                    {/* add file */}
                    <button
                      type="button"
                      className="btn btn-primary btn-flat"
                      onClick={this.toggleDialog}
                    >
                      <i className="fa fa-plus"></i>
                    </button>
                    {/* download all file */}
                    <Button
                      icon="download"
                      type="button"
                      className="pull-right btn-yellow"
                      primary={true}
                    >
                      {' '}
                      Download
                    </Button>
                    {/* download templae */}
                  </GridToolbar>
                  <Column
                    field="fileName"
                    title="File Name"
                  />
                  <Column
                    field="Uploadby"
                    title="Upload by"
                  />
                  <Column
                    field="UploadDate"
                    title="Upload Date"
                  />
                  <Column
                    field="fileName"
                    title="Remark"
                  />
                </Grid>
              </div>      
            </div>
          </div>
        </div>
        <div className="boxlist">
          <div className="headtext">ตารางการอนุมัติ (Approval Table)</div>
          <div className="body-content">
            <div className="row">
              <div className="col-md-12 k-mt-10"><p className="subheadtext">Contract Document registeation/เอกสารประกอบการทำสัญญาเช่า<br/></p></div>
              <div className="col-md-12">
                <table className="table table-bordered ">
                  <thead>
                    <tr>
                      <th scope="col"><p>No.</p></th>
                      <th scope="col"><p>Prosition</p></th>
                      <th scope="col"><p>Approver</p></th>
                      <th scope="col"><p>Action</p></th>
                      <th scope="col"><p>Date</p></th>
                    </tr>
                  </thead>
                  <tbody>
                    
                  </tbody>
                </table>
              </div>      
            </div>
          </div>
        </div>
        <div className="boxlist">
          <div className="headtext">แสดงความคิดเห้น (Comment)</div>
          <div className="body-content">
            <div className="row">
              <div className="col-md-3">
                Comment (แสดงความคิดเห็น) :
              </div> 
              <div className="col-md-9">
                <Editor
                  tools={[
                    [Bold, Italic, Underline, Strikethrough],
                    [AlignLeft, AlignCenter, AlignRight, AlignJustify],
                    [Indent, Outdent],
                    [OrderedList, UnorderedList],
                    FontSize,
                    FontName,
                    FormatBlock,
                    [Undo, Redo],
                    [Link, Unlink, InsertImage, ViewHtml],
                    [InsertTable],
                  ]}
                  contentStyle={{
                    height: 200,
                  }}
                />
              </div> 
              
            </div>
          </div>
        </div>
        <div className="">
          <div className="body-content">
            <div className="row">
              <div className="col-md-12">
                <div className="buttomsubbit k-mt-5">
                  <Button className="btn savedraft">Save Draft</Button>
                  <Button className="btn btn-success" >Submit</Button>
                  <Button className="btn btn-secondary" >Close</Button>
                </div>
              </div>  
            </div>
          </div>
        </div>
               
      </div>
    );
  }
}