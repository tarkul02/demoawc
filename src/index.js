import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import '@progress/kendo-theme-bootstrap/dist/all.css';
import './theme/theme.css'
import './css/global.css'
import './css/font.css'
import './css/custom.css'
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.bundle.min';



const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);
